# nuxeo-cluster

コマンド  

```
# イメージ構築
./build-images.sh

# 起動
./start_and_wait.sh

停止
docker-compose down
```

### deploy/conf  
nuxeo.confに追記する項目を記載する  

### deploy/instance.clid
CLIDの記述をする  
※最後付近の -- は改行して記述する  
```
xxxxxxxxxxxxxx
00000000-0000-0000-0000-000000000000
<description>
```

### deploy/mp-list
インストールするアドオン名を列挙する

### deploy/mp-add/****.zip
zipで固めたアドオンをインストールできる
